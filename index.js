module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser', 
    extraFileExtensions: ['.vue', 'ts', 'tsx'] 
  },
  extends: [
    'plugin:vue/vue3-essential',
    'plugin:vue/vue3-strongly-recommended',
    'plugin:vue/vue3-recommended',
    'airbnb-base',
    'plugin:@typescript-eslint/recommended',
    ...[
      './rules/base',
      './rules/typescript', 
    ].map(require.resolve)
  ],
  plugins: ['@typescript-eslint'],
};
