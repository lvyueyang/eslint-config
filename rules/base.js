module.exports = {
  rules: {
    /** 单引号 */
    quotes: [2, 'single'],
    /** 对象字面量中冒号的前后空格 */
    'key-spacing': [2, { beforeColon: false, afterColon: true }],
    /** 大括号前后空格 */
    'object-curly-spacing': ['error', 'always'],
    /** 指定数组的元素之间要以空格隔开(,后面)， never参数：[ 之前和 ] 之后不能带空格，always参数：[ 之前和 ] 之后必须带空格 */
    'accessor-pairs': 2,
    /** 不允许分号结尾 */
    semi: [2, 'never'],
    'no-shadow': 0,
    'consistent-return': 0,
    'no-restricted-syntax': 1,
    /** 关闭导入/无无关依赖项 */
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 0,
    'import/prefer-default-export': 0,
    'import/no-unresolved': 0,
    'max-len': ["error", 180]
  }
}
