module.exports = {
  rules: {
    'spaced-comment': 'off',
    'require-jsdoc:': 'off',
    'valid-jsdoc': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/member-ordering': 'error',
    '@typescript-eslint/no-array-constructor': 'error',
    '@typescript-eslint/no-namespace': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/prefer-namespace-keyword': 'off',
    '@typescript-eslint/type-annotation-spacing': 'error',
    '@typescript-eslint/no-use-before-define': 'error',
    '@typescript-eslint/indent': [
      0,
      2,
      {
        FunctionDeclaration: {
          parameters: 'off',
        },
        FunctionExpression: {
          parameters: 'off',
        },
      },
    ],
    '@typescript-eslint/array-type': 'error',
    '@typescript-eslint/await-thenable': 'off',
    '@typescript-eslint/ban-types': [
      'error',
      {
        extendDefaults: true,
        types: {
          Function: false,
        },
      },
    ],
    '@typescript-eslint/consistent-type-definitions': 'error',
    '@typescript-eslint/method-signature-style': [
      'error',
      'property',
    ],
    '@typescript-eslint/no-extra-non-null-assertion': 'error',
    '@typescript-eslint/no-floating-promises': 'off',
    '@typescript-eslint/no-for-in-array': 'error',
    // '@typescript-eslint/no-implied-eval': 'error',
    '@typescript-eslint/no-inferrable-types': 'error',
    '@typescript-eslint/no-misused-new': 'error',
    '@typescript-eslint/no-misused-promises': [
      'off',
      {
        checksConditionals: false,
      },
    ],
    '@typescript-eslint/no-non-null-asserted-optional-chain': 'error',
    '@typescript-eslint/no-non-null-assertion': 'error',
    '@typescript-eslint/no-this-alias': [
      'error',
      {
        allowDestructuring: true,
        allowedNames: [
          'self',
        ],
      },
    ],
    // '@typescript-eslint/no-unnecessary-type-assertion': 'error',
    '@typescript-eslint/no-var-requires': 'error',
    '@typescript-eslint/prefer-as-const': 'warn',
    '@typescript-eslint/triple-slash-reference': 'error',
    // '@typescript-eslint/unbound-method': 'error',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-extra-semi': 'error',
    '@typescript-eslint/camelcase': 'off',
  },
}
